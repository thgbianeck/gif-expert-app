// import React, { useState, useEffect } from "react";
import React from "react";
import { useFetchGifs } from "../hooks/useFetchGifs";
import { GifGridItem } from "./GifGridItem";
import { CardColumns, Button, Spinner } from "react-bootstrap";

export const GifGrid = ({ category }) => {
  const { data: images, loading } = useFetchGifs(category);

  return (
    <>
      <h2 className="animate__animated animate__fadeIn">{category}</h2>
      {loading && (
        <div className="text-center animate__animated animate__fadeIn">
          <Spinner animation="grow" variant="success" />{" "}
          <Spinner animation="grow" variant="success" />{" "}
          <Spinner animation="grow" variant="success" />
        </div>
      )}
      <CardColumns>
        {images.map((img) => (
          <GifGridItem key={img.id} {...img} />
        ))}
      </CardColumns>
    </>
  );
};
