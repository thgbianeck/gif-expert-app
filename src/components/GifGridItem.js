import React from "react";
import { Card } from "react-bootstrap";

export const GifGridItem = ({ title, url }) => {
  return (
    <Card className="animate__animated animate__fadeIn">
      <Card.Img variant="top" src={url} alt={title} />
      <Card.Body>
        <Card.Title className="animate__animated animate__fadeIn">
          {title}
        </Card.Title>
        {/* <Card.Text>
              This is a longer card with supporting text below as a natural
              lead-in to additional content. This content is a little bit
              longer.
            </Card.Text> */}
      </Card.Body>
    </Card>
  );
};
