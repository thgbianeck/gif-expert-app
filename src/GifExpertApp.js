import React, { useState } from "react";
import AddCategory from "./components/AddCategory";
import { GifGrid } from "./components/GifGrid";
import "bootstrap/dist/css/bootstrap.min.css";
import "animate.css";
import { Container, Jumbotron } from "react-bootstrap";

const GifExpertApp = () => {
  const [categories, setCategories] = useState(["Mandalorian"]);

  return (
    <Container>
      <Jumbotron className="animate__animated animate__fadeIn">
        <h1>GifExpertApp</h1>
        <p>Esta é uma aplicação para testes utilizando React.</p>
      </Jumbotron>
      <AddCategory setCategories={setCategories} />
      <hr />
      {categories.map((category) => (
        <GifGrid key={category} category={category} />
      ))}
    </Container>
  );
};

export default GifExpertApp;
